# The eggplant and peach emoji in all the skin tone variants.

This is based on the SVG files from the [twemoji](https://github.com/twitter/twemoji) emoji set. 

I used the 
[eggplant](https://github.com/twitter/twemoji/blob/master/assets/svg/1f346.svg) and 
[peach](https://github.com/twitter/twemoji/blob/master/assets/svg/1f351.svg) emoji as a basis.

I just changed the color in the SVG file according to skin tones. 

|             | Main    | Accent  | Source of the color                                                                  |
|-------------|---------|---------|--------------------------------------------------------------------------------------|
|skin neutral | #FFDC5D | #EF9645 | [handemoji](https://github.com/twitter/twemoji/blob/master/assets/svg/270b.svg)      |
|skin tone 2  | #F3D2A2 | #D2A077 | [handemoji skin skin tone 1](https://github.com/twitter/twemoji/blob/master/assets/svg/270b-1f3fc.svg)|
|skin tone 1  | #F7DECE | #E0AA94 | [handemoji skin skin tone 2](https://github.com/twitter/twemoji/blob/master/assets/svg/270b-1f3fb.svg)|
|skin tone 3  | #D5AB88 | #B78B60 | [handemoji skin skin tone 3](https://github.com/twitter/twemoji/blob/master/assets/svg/270b-1f3fd.svg)|
|skin tone 4  | #AF7E57 | #90603E | [handemoji skin skin tone 4](https://github.com/twitter/twemoji/blob/master/assets/svg/270b-1f3fe.svg)|
|skin tone 5  | #7C533E | #583529 | [handemoji skin skin tone 5](https://github.com/twitter/twemoji/blob/master/assets/svg/270b-1f3ff.svg)|


# They look like this

## Eggplant

![skin tone fruit](render_72px/eggplant.png)
![skin tone 0](render_72px/eggplant-0.png)
![skin tone 1](render_72px/eggplant-1.png)
![skin tone 2](render_72px/eggplant-2.png)
![skin tone 3](render_72px/eggplant-3.png)
![skin tone 4](render_72px/eggplant-4.png)
![skin tone 5](render_72px/eggplant-5.png)

## Peach

![skin tone fruit](render_72px/peach.png)
![skin tone 0](render_72px/peach-0.png)
![skin tone 1](render_72px/peach-1.png)
![skin tone 2](render_72px/peach-2.png)
![skin tone 3](render_72px/peach-3.png)
![skin tone 4](render_72px/peach-4.png)
![skin tone 5](render_72px/peach-5.png)
